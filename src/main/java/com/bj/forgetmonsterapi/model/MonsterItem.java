package com.bj.forgetmonsterapi.model;

import com.bj.forgetmonsterapi.enums.MonsterType;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MonsterItem {
    private Long id;
    private String monsterName;
    private String imgUrl;
    private MonsterType monsterType;
}
