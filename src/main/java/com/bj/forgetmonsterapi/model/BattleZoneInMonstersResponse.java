package com.bj.forgetmonsterapi.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BattleZoneInMonstersResponse {
    private BattleZoneInMonsterItem monster1;
    private BattleZoneInMonsterItem monster2;
}
