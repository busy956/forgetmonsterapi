package com.bj.forgetmonsterapi.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CommonResult { // 일반적인 응답
    private String msg;
    private Integer code;
}
