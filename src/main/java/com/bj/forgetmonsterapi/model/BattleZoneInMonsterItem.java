package com.bj.forgetmonsterapi.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BattleZoneInMonsterItem {
    private Long monsterStageId;
    private Long monsterId;
    private String monsterName;
    private Integer monsterHp;
    private Integer monsterAttack;
    private Integer monsterDefence;
    private Integer monsterSpeed;
    //ctrl + r로 한번에 바꾸기
}