package com.bj.forgetmonsterapi.model;

import com.bj.forgetmonsterapi.enums.MonsterType;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MonsterResponse {
    private Long id;
    private String monsterName;
    private String imgUrl;
    private Integer monsterHp;
    private Integer monsterAttack;
    private Integer monsterDefence;
    private Integer monsterSpeed;
    private MonsterType monsterType;
}
