package com.bj.forgetmonsterapi.model;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ListResult<T> extends CommonResult { // 일반적인 응답을 상속받았다
    private List<T> list;
    private Integer totalCount; // 리스트 내의 아이템 갯수
}
