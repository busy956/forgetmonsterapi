package com.bj.forgetmonsterapi.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class BattleZone {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @JoinColumn(name = "monsterId", nullable = false)
    @ManyToOne(fetch = FetchType.EAGER)
    private Monster monster;
}
