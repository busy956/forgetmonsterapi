package com.bj.forgetmonsterapi.entity;

import com.bj.forgetmonsterapi.enums.MonsterType;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class Monster {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 20)
    private String monsterName;

    @Column(nullable = false)
    private String imgUrl;

    @Column(nullable = false)
    private Integer monsterHp;

    @Column(nullable = false)
    private Integer monsterAttack;

    @Column(nullable = false)
    private Integer monsterDefence;

    @Column(nullable = false)
    private Integer monsterSpeed;

    @Column(nullable = false)
    @Enumerated(value = EnumType.STRING)
    private MonsterType monsterType;
}
