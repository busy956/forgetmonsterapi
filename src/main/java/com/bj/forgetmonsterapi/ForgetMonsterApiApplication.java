package com.bj.forgetmonsterapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ForgetMonsterApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(ForgetMonsterApiApplication.class, args);
    }

}
