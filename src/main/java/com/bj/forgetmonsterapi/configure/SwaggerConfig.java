package com.bj.forgetmonsterapi.configure;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import lombok.RequiredArgsConstructor;
import org.springdoc.core.models.GroupedOpenApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@OpenAPIDefinition(
        info = @Info(title = "monster App",
                description = "forget monster의 치열한 싸움",
                version = "v1"))
@RequiredArgsConstructor
@Configuration

public class SwaggerConfig {
    @Bean
    public GroupedOpenApi healthOpenApi() {
        String[] paths = {"/v1/**"};

        return GroupedOpenApi.builder()
                .group("포겟몬스터 API v1")
                .pathsToMatch(paths)
                .build();
    }
}
