package com.bj.forgetmonsterapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum MonsterType {
    FIRE("불"),
    WATER("물"),
    GRASS("풀"),
    GROUND("땅");

    private final String Type;
}
