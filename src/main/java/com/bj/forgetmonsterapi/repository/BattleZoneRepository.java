package com.bj.forgetmonsterapi.repository;

import com.bj.forgetmonsterapi.entity.BattleZone;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BattleZoneRepository extends JpaRepository<BattleZone, Long> {
}
