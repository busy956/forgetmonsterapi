package com.bj.forgetmonsterapi.repository;

import com.bj.forgetmonsterapi.entity.Monster;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MonsterRepository extends JpaRepository<Monster, Long> {
}
