package com.bj.forgetmonsterapi.controller;

import com.bj.forgetmonsterapi.entity.Monster;
import com.bj.forgetmonsterapi.model.CommonResult;
import com.bj.forgetmonsterapi.service.BattleZoneService;
import com.bj.forgetmonsterapi.service.MonsterService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/BattleZone")
public class BattleZoneController {
    private final BattleZoneService battleZoneService;
    private final MonsterService monsterService;

    @PostMapping("/stage/in/monster-id/{monsterId}") //
    public CommonResult setMonsterBattleZoneIn(@PathVariable long monsterId) throws Exception { // 몬스터의 id를 통해 몬스터의 정보를 가져온다 그 후 대기실에 몬스터를 저장한다
        Monster monster = monsterService.getData(monsterId);
        battleZoneService.setBattleZone(monster);

        CommonResult result = new CommonResult();
        result.setMsg("성공하셨습니다");
        result.setCode(0);

        return result;
    }

    @GetMapping("/current/state")
    public
}
