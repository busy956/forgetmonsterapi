package com.bj.forgetmonsterapi.service;

import com.bj.forgetmonsterapi.entity.Monster;
import com.bj.forgetmonsterapi.model.MonsterItem;
import com.bj.forgetmonsterapi.model.MonsterRequest;
import com.bj.forgetmonsterapi.model.MonsterResponse;
import com.bj.forgetmonsterapi.repository.MonsterRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MonsterService {
    private final MonsterRepository monsterRepository;

    public void setMonster(MonsterRequest request) {
        // 새로운 데이터를 등록하는데 등록하려면 새로운 entity 모양 틀이 필요하다
        Monster addData = new Monster(); // 데이터를 새로 넣는 거니까 addData
        // 고객이 작성한 양식을 가져와 새 틀에 집어넣는다
        addData.setMonsterName(request.getMonsterName());
        addData.setImgUrl(request.getImgUrl());
        addData.setMonsterHp(request.getMonsterHp());
        addData.setMonsterAttack(request.getMonsterAttack());
        addData.setMonsterDefence(request.getMonsterDefence());
        addData.setMonsterSpeed(request.getMonsterSpeed());
        addData.setMonsterType(request.getMonsterType());

        // 레포지토리에게 틀을 주면서 저장하라고 한다
        monsterRepository.save(addData);
    }

    public List<MonsterItem> getMonsters() { // 복수 R 리스트
        List<Monster> originList = monsterRepository.findAll();
        // entity 모양의 리스트를 레포지토리에게 가져오라고 한다
        List<MonsterItem> result = new LinkedList<>();
        // 우리가 원하는 결과인 아이템 리스트의 틀을 준비한다
        for (Monster monster : originList) { // entity의 새 틀이 다 채워진 틀로 될 때까지
            MonsterItem additem = new MonsterItem(); // 아이템의 새 틀을 만든다
            additem.setId(monster.getId()); // 새 틀안에 entity안의 데이터를 집어넣는다
            additem.setMonsterName(monster.getMonsterName());
            additem.setImgUrl(monster.getImgUrl());
            additem.setMonsterType(monster.getMonsterType());

            result.add(additem); // 아이템 리스트 틀 안에 데이터를 집어넣은 아이템의 데이터를 넣는다
        }
        return result;
    }

    public MonsterResponse getMonster(long id) { // 단수 R 상세보기 - 그 아이템의 해당하는 id가 필요하다
        Monster originData = monsterRepository.findById(id).orElseThrow(); // entity 안에 있는 id 찾아오고 못 찾으면 던진다!
        MonsterResponse response = new MonsterResponse(); // response 모양의 새 틀 만들기
        response.setMonsterName(originData.getMonsterName());
        response.setImgUrl(originData.getImgUrl());
        response.setMonsterHp(originData.getMonsterHp());
        response.setMonsterAttack(originData.getMonsterAttack());
        response.setMonsterDefence(originData.getMonsterDefence());
        response.setMonsterSpeed(originData.getMonsterSpeed());
        response.setMonsterType(originData.getMonsterType());

        return response;
    }

    public Monster getData (long id) { // id를 이용해서 몬스터를 찾아오는 메서드
       return monsterRepository.findById(id).orElseThrow();
    }

    public void delMonster(Long id) { // id를 이용해서 몬스터를 삭제한
        monsterRepository.deleteById(id);
    }
}
