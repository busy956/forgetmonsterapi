package com.bj.forgetmonsterapi.service;

import com.bj.forgetmonsterapi.entity.BattleZone;
import com.bj.forgetmonsterapi.entity.Monster;
import com.bj.forgetmonsterapi.model.BattleZoneInMonsterItem;
import com.bj.forgetmonsterapi.model.BattleZoneInMonstersResponse;
import com.bj.forgetmonsterapi.repository.BattleZoneRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor //종속성 자동주입
public class BattleZoneService {
    private final BattleZoneRepository battleZoneRepository;

    public void setBattleZone (Monster monster) throws Exception {
        //몬스터 둘 이상 결투장 진입이 불가능합니다. 현재 몇마리의 몬스터가 결투장에 있는지 알아야 한다.
        // 결투장에 진입한 몬스터 리스트를 가져온다.
        List<BattleZone> checkList = battleZoneRepository.findAll();
        //만약.... checkList의 갯수가 2마리 이상이면 던지자.
        if (checkList.size() >= 2) throw new Exception();

        BattleZone battleZone = new BattleZone();
        battleZone.setMonster(monster);

        battleZoneRepository.save(battleZone);
    }

    public void delStageInMonster(long id) {
        battleZoneRepository.deleteById(id);
    }

    public BattleZoneInMonstersResponse getCurrentState() {
        // 일단 결투장에 진입한 몬스터 리스트 다 가져와...
        // 근데 넣을때 최대 2마리만 넣을 수 있게 해놨으니까
        // 경우의 수는 0개, 1개, 2개
        List<BattleZone> checkList = battleZoneRepository.findAll();

        // BattleZoneInMonstersResponse 모양으로 무조건 줘야한다.
        BattleZoneInMonstersResponse response = new BattleZoneInMonstersResponse();
        // 이렇게 하면 안쪽에 칸들은 두칸 다 null.
        // 두칸 다 null이란 건 0개일때 이미 처리하고 간다는 거.

        if (checkList.size() == 2) {
            // 리스트에서 0번째 요소를 가져온다.
            // BattleZone monster1origin = checkList.get(0);

            response.setMonster1(convertMonsterItem(checkList.get(0)));
            response.setMonster1(convertMonsterItem(checkList.get(1)));
        } else if (checkList.size() == 1) {
            response.setMonster1(convertMonsterItem(checkList.get(0)));
        }
        return response;
    }

    private BattleZoneInMonsterItem convertMonsterItem(BattleZone battleZone) {
        BattleZoneInMonsterItem monsterItem = new BattleZoneInMonsterItem();
        monsterItem.setMonsterStageId(battleZone.getId());
        monsterItem.setMonsterId(battleZone.getMonster().getId());
        monsterItem.setMonsterName(battleZone.getMonster().getMonsterName());
        monsterItem.setMonsterHp(battleZone.getMonster().getMonsterHp());
        monsterItem.setMonsterAttack(battleZone.getMonster().getMonsterAttack());
        monsterItem.setMonsterDefence(battleZone.getMonster().getMonsterDefence());
        monsterItem.setMonsterSpeed(battleZone.getMonster().getMonsterSpeed());

        return monsterItem;

    }
}
